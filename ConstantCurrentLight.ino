char junk = ' ';
#define LIGH_PWM_1 4
#define LIGH_PWM_2 5
#define LIGH_PWM_3 6
#define LIGH_PWM_4 7

#define LIGH_CURRENT_1 A8
#define LIGH_CURRENT_2 A9
#define LIGH_CURRENT_3 A10
#define LIGH_CURRENT_4 A11

#define LIGHT_SENSOR_1 A12
#define LIGHT_SENSOR_2 A13

#define MOVEMENT_SENSOR_1 30
#define MOVEMENT_SENSOR_2 31

int lightPwm1 = 0;
int lightPwm2 = 0;
int lightPwm3 = 0;
int lightPwm4 = 0;

int lightSetpoint1 = 0;
int lightSetpoint2 = 0;
int lightSetpoint3 = 0;
int lightSetpoint4 = 0;

int lightCurrent1 = 0;
int lightCurrent2 = 0;
int lightCurrent3 = 0;
int lightCurrent4 = 0;

int lightSensor1 = 0; 
int lightSensor2 = 0;
int lightMeasurements = 0;

int movementSensor1 = 0;
int movementSensor2 = 0;


long sum1 = 0;
long sum2 = 0;
long sum3 = 0;
long sum4 = 0;

long lightSum1 = 0;
long lightSum2 = 0;

char inputBuffer[128];
char outputBuffer[500];

int calculatePwm(int pwm, int measured, int setpoint){
  if(setpoint == 0){
    pwm = 0;
  }else{
    if(setpoint > measured + 2) pwm += 5;
    else if(setpoint >= measured +1) pwm += 1;
    else if(setpoint < measured -2) pwm -= 5;
    else if(setpoint <= measured - 1) pwm -= 1;
  }

  if(pwm > 255) return 255;

  return pwm;
}

void setup() {
  analogReference(EXTERNAL);
  
  pinMode(MOVEMENT_SENSOR_1, INPUT);
  pinMode(MOVEMENT_SENSOR_2, INPUT);

  Serial.begin(115200);
}


void loop() {
  if (Serial.available() > 0) {
    int i = 0; 
    while (Serial.available() > 0){
      inputBuffer[i] = Serial.read();
      i++;
    }
    inputBuffer[i] = '\0';

    sscanf(inputBuffer, "{\"l1\": %d,\"l2\": %d,\"l3\": %d,\"l4\": %d}", &lightSetpoint1, &lightSetpoint2, &lightSetpoint3, &lightSetpoint4);
  }

  lightSum1 += analogRead(LIGHT_SENSOR_1);
  lightSum2 += analogRead(LIGHT_SENSOR_2);
  lightMeasurements++;

  movementSensor1 = digitalRead(MOVEMENT_SENSOR_1) ? 1 : 0;
  movementSensor2 = digitalRead(MOVEMENT_SENSOR_2) ? 1 : 0;

  if(lightMeasurements == 20){
    lightMeasurements = 0;
    lightSensor1 = lightSum1 / 10; 
    lightSensor2 = lightSum2 / 10;
    lightSum1 = 0;
    lightSum2 = 0;
  }
  
  // read the analog in value:
  sum1 = sum2 = sum3 = sum4 = 0;
  for(int i = 0; i < 100; i++){
    sum1 += analogRead(LIGH_CURRENT_1);  
    sum2 += analogRead(LIGH_CURRENT_2);  
    sum3 += analogRead(LIGH_CURRENT_3);  
    sum4 += analogRead(LIGH_CURRENT_4);  
    
    delay(1);
  }

  lightCurrent1 = sum1 / 1000;;
  lightCurrent2 = sum2 / 1000;;
  lightCurrent3 = sum3 / 1000;;
  lightCurrent4 = sum4 / 1000;;

  lightPwm1 = calculatePwm(lightPwm1, lightCurrent1, lightSetpoint1);
  lightPwm2 = calculatePwm(lightPwm2, lightCurrent2, lightSetpoint2);
  lightPwm3 = calculatePwm(lightPwm3, lightCurrent3, lightSetpoint3);
  lightPwm4 = calculatePwm(lightPwm4, lightCurrent4, lightSetpoint4);

  analogWrite(LIGH_PWM_1, lightPwm1);
  analogWrite(LIGH_PWM_2, lightPwm2);
  analogWrite(LIGH_PWM_3, lightPwm3);
  analogWrite(LIGH_PWM_4, lightPwm4);
  
  sprintf(
    outputBuffer,
    "{\"lightSensor1\": %d, \"lightSensor2\": %d, \"movementSensor1\": %d, \"movementSensor2\": %d, \"lightSetpoint1\": %d, \"lightSetpoint2\": %d, \"lightSetpoint3\": %d, \"lightSetpoint4\": %d, \"lightCurrent1\": %d, \"lightCurrent2\": %d,\"lightCurrent3\": %d,\"lightCurrent4\": %d, \"lightPwm1\": %d, \"lightPwm2\": %d, \"lightPwm3\": %d, \"lightPwm4\": %d}",
    lightSensor1, lightSensor2, movementSensor1, movementSensor2, lightSetpoint1, lightSetpoint2, lightSetpoint3, lightSetpoint4, lightCurrent1, lightCurrent2, lightCurrent3, lightCurrent4, lightPwm1, lightPwm2, lightPwm3, lightPwm4
  );
  Serial.println(outputBuffer);
  
  delay(10);
}
